# ----------------------------------
# Cluster Dendrograms - Shiny App
# Data Visualization
# 10/26/2017
# David Graf
# ----------------------------------

#install.packages("shiny")
library("shiny")

ui <- fluidPage(
  fluidRow(column(2, offset = 5,sliderInput(inputId = "num",
              label = "Choose a number",
              value = 32, min = 1, max = 32))),
  fluidRow(column(6, offset = 3, plotOutput("dendro")))
)

server <- function(input, output) {
  output$dendro <- renderPlot({
    mycars <- mtcars[1:input$num,c(1:2,4,6:7,9)]  
    d <- dist(as.matrix(mycars))
    hc <- hclust(d)
    plot(hc, main="Similarity of Cars", ylab="Distance", xlab="")
  })
}

shinyApp(server = server, ui = ui)